var path = require('path');
var src = path.join(__dirname, '..', 'src');
var dest = path.join(__dirname, '..', 'out');

module.exports = {
  
  js: [{
    browserify: {
      cache: {},
      packageCache: {},
      fullPaths: false,
      entries: path.join(src, 'js/app.js'),
      extensions: ['.js', '.json', '.jsx', '.babel']
    },
    dest: path.join(dest, 'assets'),
    source: 'app.js'
  }],

  markup: [{
    hbsOptions: {
      batch: [
        './src/handlebars/_',
        './src/handlebars/pages'
      ],
      ignorePartials: true,
    },
    htmlmin: {
      minifyJS: true, 
      collapseWhitespace: true,
      minifyCSS: true
    },
    src: path.join(src, 'handlebars', 'index.handlebars'),
    dest: dest,
    rename: 'index.html'
  }, {
    hbsOptions: {
      batch: [
        './src/handlebars/_',
        './src/handlebars/pages'
      ],
      ignorePartials: true,
    },
    htmlmin: {
      minifyJS: true, 
      collapseWhitespace: true,
      minifyCSS: true
    },
    src: path.join(src, 'handlebars', 'list.handlebars'),
    dest: dest,
    rename: 'list.html'
  }, {
    hbsOptions: {
      batch: [
        './src/handlebars/_',
        './src/handlebars/pages'
      ],
      ignorePartials: true,
    },
    htmlmin: {
      minifyJS: true, 
      collapseWhitespace: true,
      minifyCSS: true
    },
    src: path.join(src, 'handlebars', 'empty-cart.handlebars'),
    dest: dest,
    rename: 'empty-cart.html'
  }, {
    hbsOptions: {
      batch: [
        './src/handlebars/_',
        './src/handlebars/pages'
      ],
      ignorePartials: true,
    },
    htmlmin: {
      minifyJS: true, 
      collapseWhitespace: true,
      minifyCSS: true
    },
    src: path.join(src, 'handlebars', 'cart.handlebars'),
    dest: dest,
    rename: 'cart.html'
  }, {
    hbsOptions: {
      batch: [
        './src/handlebars/_',
        './src/handlebars/pages'
      ],
      ignorePartials: true,
    },
    htmlmin: {
      minifyJS: true, 
      collapseWhitespace: true,
      minifyCSS: true
    },
    src: path.join(src, 'handlebars', 'order.handlebars'),
    dest: dest,
    rename: 'order.html'
  }, {
    hbsOptions: {
      batch: [
        './src/handlebars/_',
        './src/handlebars/pages'
      ],
      ignorePartials: true,
    },
    htmlmin: {
      minifyJS: true, 
      collapseWhitespace: true,
      minifyCSS: true
    },
    src: path.join(src, 'handlebars', 'item.handlebars'),
    dest: dest,
    rename: 'item.html'
  }, {
    hbsOptions: {
      batch: [
        './src/handlebars/_',
        './src/handlebars/pages'
      ],
      ignorePartials: true,
    },
    htmlmin: {
      minifyJS: true, 
      collapseWhitespace: true,
      minifyCSS: true
    },
    src: path.join(src, 'handlebars', 'collection.handlebars'),
    dest: dest,
    rename: 'collection.html'
  }],

  css: [{
    src: path.join(src, 'scss/style.scss'),
    dest: path.join(dest, 'assets'),
    prefixer: {
      browsers: ['> 0%'],
      cascade: false
    },
    rename: 'style.css'
  }],

  browserSync: {
    server: {
      baseDir: dest
    },
    files: [
      path.join(dest, '**')
    ],
    port: 4000,
    open: false
  },

  watch: [{
    path: path.join(src, 'scss/**'),
    name: ['scss']
  }, {
    path: path.join(src, '**/*.handlebars'),
    name: ['markup']
  }],

  lint: {
    scss: {
      src: [
        path.join(src, 'scss/**/*.scss'),
        path.join('!', src, 'scss/helpers/**/*.scss'),
        path.join('!', src, 'scss/base/**/*.scss'),
      ],
      dest: 'scss-report.xml',
      config: path.join(__dirname, '..', 'scss-lint.yml')
    },
    js: {}
  }
};
