var gulp = require('gulp'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    babelify = require('babelify'),
    handleErrors = require('../util/handleErrors'),
    bundleLogger = require('../util/bundleLogger'),
    uglify = require('gulp-uglify'),
    source = require('vinyl-source-stream'),
    streamify = require('gulp-streamify'),
    gulpif = require('gulp-if'),
    minimist = require('minimist'),
    config = require('../config').js,
    def, options;

def = {
  string: 'env',
  default: {
    env: process.env.NODE_ENV || 'dev'
  }
};
options = minimist(process.argv.slice(2), def);

gulp.task('js', function() {
  return config.map(function(config) {
    var bundler, bundle;

    bundler = browserify(config.browserify) // browserify bundle
      .transform(babelify) // babelify bundle

    if (global.isWatchify) bundler = watchify(bundler); // if not prod set watchify cache

    bundle = function() {
      bundleLogger.start(config.browserify.entries); // start logging

      return bundler
        .bundle()
        .on('error', handleErrors) // handle errors
        .pipe(source(config.source)) // set source dest
        .pipe(gulpif(options.env === 'prod', streamify(uglify()))) // uglify if prod
        .pipe(gulp.dest(config.dest)) // write to dest
        .on('end', function() {
          bundleLogger.end(config.browserify.entries); // log that everything is bundled
        });
    };

    bundler.on('update', bundle); // set listener to bundle

    return bundle();
  });
});
