var gulp = require('gulp'),
    fs = require('fs'),
    handlebars = require('gulp-compile-handlebars'),
    handleErrors = require('../util/handleErrors'),
    rename = require('gulp-rename'),
    htmlmin = require('gulp-htmlmin'),
    gulpif = require('gulp-if'),
    minimist = require('minimist'),
    config = require('../config').markup,
    def, options;

def = {
  string: 'env',
  default: {
    env: process.env.NODE_ENV || 'dev'
  }
};
options = minimist(process.argv.slice(2), def);

gulp.task('markup', function() {
  return config.map(function(config) {
    return gulp.src(config.src)
      .pipe(handlebars({}, config.hbsOptions)) // precompile handlebars
      .on('error', handleErrors) // handle errors
      .pipe(gulpif(options.env === 'prod', htmlmin(config.htmlmin))) // minify if production
      .pipe(rename(config.rename)) // rename
      .pipe(gulp.dest(config.dest)); // write to dest
  });
});
