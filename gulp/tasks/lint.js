var gulp = require('gulp'),
    scsslint = require('gulp-scss-lint'),
    lint = require('../config').lint;

gulp.task('lint-scss', function() {
  return gulp.src(lint.scss.src)
    .pipe(scsslint({
      reporterOutput: lint.scss.dest,
      config: lint.scss.config
    }));
});

gulp.task('lint', ['lint-scss']);
