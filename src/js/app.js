import $ from 'jquery';
window.$ = window.jQuery = $;
require('jquery-touchswipe');

function getSupportedCSSProperty(arr) {
  const doc = document.documentElement;
  const len = arr.length;
 
  for (let i = 0; i < len; i++) {
    if (arr[i] in doc.style) {
      return arr[i];
    }
  }
}

const transform = getSupportedCSSProperty(['transform', 'MozTransform', 'WebkitTransform', 'msTransform', 'OTransform']);

// Mobile units slider
$(() => {
  const $sliders = $('.js-units-slider')

  $sliders.each(function() {
    const $slider = $(this);
    const $container = $slider.find('.js-units-slider__container');
    const $slides = $slider.find('.js-units-slider__slide');
    const len = $slides.length;
    let activeIndex = 0;

    function onSwipe(e, dir) {
      let next = dir === 'left' ? activeIndex + 1 : activeIndex - 1;
      if (next < 0) next = len - 1;
      if (next > len - 1) next = 0;
      activeIndex = next;
      onChange();
    }

    function onChange() {
      $container[0].style[transform] =  `translateX(${ activeIndex * -100 }%)`;
    }

    function onResize() {
      if (window.innerWidth > 770) {
        activeIndex = 0;
        onChange();
      }
    }

    window.addEventListener('resize', onResize, false);
    $slider.swipe({ swipe: onSwipe, excludedElements: '' });
    onResize();
    onChange();
  });
});

// Dropdown
$(() => {
  const $dropdowns = $('.js-dropdown');
  const TRIGGER_ACTIVE_CLASS = 'trigger-open';
  const LIST_ACTIVE_CLASS = 'list-open';

  $dropdowns.each(function() {
    const $dropdown = $(this);
    const $list = $dropdown.find('.js-dropdown-list');
    const $trigger = $dropdown.find('.js-dropdown-trigger');
    let isOpen = false;

    $trigger.on('click', () => {
      if (isOpen) {
        $trigger.removeClass(TRIGGER_ACTIVE_CLASS);
        $list.removeClass(LIST_ACTIVE_CLASS);
        isOpen = false;
      } else {
        $trigger.addClass(TRIGGER_ACTIVE_CLASS);
        $list.addClass(LIST_ACTIVE_CLASS);
        isOpen = true;
      }
    });
  });
});

// Order inputs
$(() => {
  const $shownIfElements = $('[data-shown-if]');

  $shownIfElements.map(function() {
    const $el = $(this);
    const { select, value } = JSON.parse($el.attr('data-shown-if'));
    const $select = $(select);

    const onChange = () => $el.css('display', $select.val() === value ? 'block' : 'none');

    $select.on('change', onChange);
    onChange();
  });

});

// Article Slider
$(() => {
  const $sliders = $('.js-slider');

  $sliders.each(function() {
    const $slider = $(this);
    const [ 
      $counter, 
      $next, 
      $prev, 
      $container,
      $slides,
      $pagination
    ] = [
      $slider.find('.js-slider-index'),
      $slider.find('.js-slider-next'),
      $slider.find('.js-slider-prev'),
      $slider.find('.js-slider-container'),
      $slider.find('.js-slider-slide'),
      $slider.find('.js-slider-pagination-item')
    ];

    const len = $container.children().length;
    let activeIndex = 0;

    function updateDOM() {
      $counter ? $counter.html(`${activeIndex + 1}/${len}`) : void(0);
      $container[0].style[transform] =  `translateX(${ activeIndex * -100 }%)`;
      $pagination && $pagination.length ? $pagination.each(function(i) {
        activeIndex === i ? $(this).addClass('active') : $(this).removeClass('active')
      }) : void(0);
    }

    function onResize() {
      const width = $container.width();
      const height = $container.height();
      $slides.css({ width, height });
    }
    
    function onChange(where) {
      let next = where === 'next' ? activeIndex + 1 : activeIndex - 1;
      if (next < 0) next = len - 1;
      if (next > len - 1) next = 0;
      activeIndex = next;
      updateDOM();
    }

    $next.on('click', () => onChange('next'));
    $prev.on('click', () => onChange('prev'));
    window.addEventListener('resize', onResize, false);
    $pagination.each(function(i) {
      $(this).on('click', () => {
        activeIndex = i;
        updateDOM();
      });
    });
    onResize();
    updateDOM();
  });
});

// Social links
$(() => {
  const $links = $('.js-social-link');

  $links.each(function() {
    const $link = $(this);
    const { url, title, options } = JSON.parse($link.attr('data-window'));
    $link.on('click', (e) => {
      e.preventDefault();
      window.open(url, title, options)
    });
  });
})
